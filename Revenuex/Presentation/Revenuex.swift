//
//  Revenuex.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 17.03.2021.
//

import Foundation
import StoreKit

public class Revenuex {
    
    //MARK: Observers
    lazy var paymentObserver:PaymentObserver = {
        return DefaultPaymentObserver(paymentQueue: SKPaymentQueue.default())
    }()
    
    lazy var applicationStateObserver:ApplicationStateObserver = {
        return DefaultApplicationStateObserver()
    }()
    
    lazy var remoteConfigObserver:RemoteConfigObserver = {
        return DefaultRemoteConfigObserver()
    }()
    
    //MARK: Network Service
    var networkConfiguration:NetworkConfiguration = DefaultNetworkConfiguration()
    lazy var networkService:NetworkService = {
        let networkSessionManager:NetworkSessionManager = DefaultNetworkSessionManager()
        return DefaultNetworkService(config: networkConfiguration, sessionManager: networkSessionManager)
    }()
    
    //MARK: Network Requester
    lazy var requester:NetworkRequester = {
        let requesterErrorLogger:RequesterErrorLogger = DefaultRequesterErrorLogger()
        let requesterErrorResolver:RequesterErrorResolver = DefaultRequesterErrorResolver()
        return DefaultNetworkRequester (networkService: networkService, errorResolver: requesterErrorResolver,errorLogger: requesterErrorLogger)
    }()
    
    //MARK: User Repository
    lazy var userRepository:RevenuexUserRepository = {
        let userStorage:RevenuexUserStorage = CoreDataRevenueXUserStorage(coreDataStorage: CoreDataStorage.shared)
        return DefaultRevenuexUserRepository(requester: requester, cache: userStorage)
    }()
    
    //MARK: Attributes Repository
    lazy var attributesRepository:AttributesRepository = {
        let attributesStorage:AttributesStorage = CoreDataAttributesStorage(coreDataStorage: CoreDataStorage.shared)
        return DefaultAttributesRepository(requester: requester, cache: attributesStorage)
    }()
    
    //MARK: Application Repository
    lazy var applicationRepository:ApplicationRepositorty = {
       return DefaultApplicationRepositorty(userDefaults: UserDefaults.standard)
    }()

    //MARK: Purchases Repository
    lazy var purchasesRepository:PurchasesRepository = {
        return DefaultPurchasesRepository(requester: requester)
    }()
    
    //MARK: Remote Config Repository
    lazy var remoteConfigRepository:RemoteConfigRepository = {
        let storage = DefaultRemoteConfigStorage(userDefaults: UserDefaults.standard)
        return DefaultRemoteConfigRepository(requester: requester, remoteConfigStorage: storage, configObserver: remoteConfigObserver)
    }()

    public static let shared: Revenuex = {
        return Revenuex()
    }()
     
    private init() {}
    
    internal func start() {
        
        UpdateLastOpenUseCase(userRepository: userRepository)
            .execute(completion: { (result) in
                switch result {
                case .success(_):
                    SendAllReceiptsOneTimeForFirstLaunchIfNeededUseCase(applicationRepository: self.applicationRepository,
                                                                            purchasesRepository: self.purchasesRepository)
                            .execute()
                    QueryProductsFromRemoteUseCase(purchasesRepository: self.purchasesRepository)
                        .execute ()
                    SyncAttributesUseCase(attributesRepository: self.attributesRepository)
                        .execute()
                case .failure(let error):
                    print(error)
                }
            })
        
        //applicationStateObserver.startObserving()
        self.paymentObserver.startObserving()
    }
    
}
