//
//  Revenuex+PaymentObserver.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 2.05.2021.
//

import Foundation
import StoreKit

extension Revenuex : PaymentObserverDelegate {
    
    func paymentObserverDidHandlePurchasedTransaction(transactions: [SKPaymentTransaction]) {
        
        var products:Set<String> = []
        for item in transactions {
            products.insert(item.payment.productIdentifier)
        }
        self.purchasesRepository.fetchExternalProductsFromApple(products: products, completion: { (result) in
            switch result {
            case .success(let offerings):
                for product in offerings {
                    self.purchasesRepository.sendPaymentInfo(product: product, completion: nil)
                }
                guard let receiptURL = Bundle.main.appStoreReceiptURL, let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString() else {
                        return
                }
                self.applicationRepository.setReceiptString(receipt: receiptString)
            case .failure(let error):
                print(error)
            }
        })
    }

    
}
