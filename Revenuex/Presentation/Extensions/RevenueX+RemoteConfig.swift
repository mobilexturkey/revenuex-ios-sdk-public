//
//  RevenueX+RemoteConfig.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 5.07.2021.
//

import Foundation

extension Revenuex {
    
    public func getRemoteConfig() -> RemoteConfig {
        return self.remoteConfigRepository
    }

}

extension Revenuex : RemoteConfigObserverDelegate {
    
    func configExpired() {
        FetchRemoteConfigUseCase(remoteConfigRepository: Revenuex.shared.remoteConfigRepository)
            .execute(completion: { (result) in
            switch result {
            case .success(_):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: RemoteConfigSettings.notificationKeyConfigRefreshed), object: nil, userInfo: nil)
                }
            case .failure(_):
                break
            }
        })
    }
    
}


extension RemoteConfig {
    
    public func getConfigValue(key: String) -> RemoteConfigValue? {
        return Revenuex.shared.remoteConfigRepository.configValue(forKey: key)
    }
    
    public func setConfigValue(forKey key: String, value: AnyObject?) {
        Revenuex.shared.remoteConfigRepository.setConfigValueToRepo(forKey: key, value: value)
    }
    
    public func setConfigValues(fromPlist: String) {
        Revenuex.shared.remoteConfigRepository.setConfigValuesToRepo(fromPlist: fromPlist)
    }
    
    public func getDefaultValue(key: String) -> RemoteConfigValue? {
        return Revenuex.shared.remoteConfigRepository.defaultValue(forKey: key)
    }
    
    public func setDefaultValue(forKey key: String, value: AnyObject?) {
        Revenuex.shared.remoteConfigRepository.setDefaultValueToRepo(forKey: key, value: value)
    }
    
    public func setDefaultValues(fromPlist: String) {
        Revenuex.shared.remoteConfigRepository.setDefaultValuesToRepo(fromPlist: fromPlist)
    }
    
    public func fetch(completion: @escaping ((Result<Void, Error>) -> Void)) -> Void  {
        FetchRemoteConfigUseCase(remoteConfigRepository: Revenuex.shared.remoteConfigRepository)
            .execute(completion: completion)
    }
    
}

