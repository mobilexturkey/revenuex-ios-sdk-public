//
//  Reveuex+Public.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 2.05.2021.
//

import Foundation

extension Revenuex {
    
    public func configure(with APIKey:String, observerMode:Bool = true) {
        networkConfiguration.headers["clientid"] = APIKey
        networkConfiguration.headers["user-revenueXId"] = GetRevenuexIdUseCase(applicationRepository: applicationRepository).execute()
        applicationStateObserver.delegate = self
        paymentObserver.delegate = self
        start()
    }
    
    public func setAttribute(name:String, value:String) {
        SetAttributesLocallyUseCase
            .init(attributes: [.init(key: name, value: value)],
                  attributesRepository: attributesRepository)
            .execute()
    }
    
    public func collectDeviceIdentifiers() {
        //TODO: (Level-2)
        /*
         1- Add idfa, idfv and ip calculated static properties into SystemInfo
         2- Call "SetAttributesLocallyUseCase" usecase and pass $idfa, $idfv and $ip values as parameter.
        */
        if let idfa = SystemInfo.IDFA {
            SetAttributesLocallyUseCase
                .init(attributes: [.init(key: "idfa", value: idfa)],
                      attributesRepository: attributesRepository)
                .execute()
        }
        if let idfv = SystemInfo.IDFV {
            SetAttributesLocallyUseCase
                .init(attributes: [.init(key: "idfv", value: idfv)],
                      attributesRepository: attributesRepository)
                .execute()
        }
        if let ipAdress = SystemInfo.IPAdress {
            SetAttributesLocallyUseCase
                .init(attributes: [.init(key: "ip", value: ipAdress)],
                      attributesRepository: attributesRepository)
                .execute()
        }
    }
    


}
