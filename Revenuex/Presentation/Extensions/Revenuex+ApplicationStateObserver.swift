//
//  Revenuex+ApplicationStateObserver.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 2.05.2021.
//

import Foundation

extension Revenuex : ApplicationStateObserverDelegate {

    func applicationWillEnterForeground() {
        SyncAttributesUseCase(attributesRepository: attributesRepository)
            .execute()
    }

    
}
