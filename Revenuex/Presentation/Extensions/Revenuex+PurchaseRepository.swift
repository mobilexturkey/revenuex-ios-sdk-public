//
//  Revenuex+PurchaseRepository.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 28.06.2021.
//

import Foundation

extension Revenuex {
    
    public func canMakePayment() -> Bool {
        return self.purchasesRepository.canMakePayment()
    }
 
    public func getRevenueXProducts(completion: @escaping ((Result<[RevenueXProduct], Error>) -> Void)) -> Void  {
        QueryProductsFromRemoteUseCase(purchasesRepository: purchasesRepository)
            .execute(completion: completion)
    }
    
    public func purchaseRevenueXProduct(product: RevenueXProduct, completion: @escaping ((Result<RevenueXProduct, Error>) -> Void)) -> Void  {
        PurchaseRevenuexProductUseCase(purchasesRepository: purchasesRepository, product: product)
            .execute(completion: completion)
    }
}
