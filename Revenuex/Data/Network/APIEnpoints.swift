//
//  APIEnpoints.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 19.04.2021.
//

import Foundation

struct APIEndpoints {
    
    static func sendOpenEvent() -> Endpoint<CurrentRevenuexUserDTO> {
        return Endpoint(path:"api/transactions/application-users/open-event",
                        method: .post)
    }
    
    static func sendPaymentInfo(receiptData: String, price: Double, productId: String, currency: String, period: String, isSubscription:Bool) -> Endpoint<Void> {
        
        let product: [String: Any] = [
            "productId": productId,
            "price": price,
            "period": period,
            "currency": currency
        ]
        
        let body: [String:Any] = [
            "products":[product],
            "isSubscription": isSubscription,
            "receiptData" : receiptData
        ]
        
        return Endpoint(path:"api/transactions/validate",
                        method: .post,
                        bodyParamaters: body)
    }
    
    static func sendReceiptData(receiptData: String, isSubscription:Bool) -> Endpoint<Void> {
        
        let body: [String:Any] = [
            "products":[],
            "isSubscription": isSubscription,
            "receiptData" : receiptData
        ]
        
        return Endpoint(path:"api/transactions/validate",
                        method: .post,
                        bodyParamaters: body)
    }
    
    static func syncAttirbutes(attiribues:[[String:Any]]) -> Endpoint<Void> {
        return Endpoint(path:"api/transactions/application-users/set-attributes",
                        method: .post,
                        bodyParamaters: ["attributes":attiribues])
    }
    
    static func getProductsFromRemote() -> Endpoint<[ProductResponseModel]> {
        return Endpoint(path: "api/products/all", method: .get)
        
    }
    
    static func getRemoteConfig() -> Endpoint<[String:AnyHashable]> {
        return Endpoint(path: "api/config/remote-config", method: .get)
    }
    
    

}
