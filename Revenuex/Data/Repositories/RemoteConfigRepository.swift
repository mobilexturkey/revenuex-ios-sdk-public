//
//  RemoteConfigRepository.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 1.07.2021.
//

import Foundation

public protocol RemoteConfig {
    
    var configSettings: RemoteConfigSettings {get set}
    var lastFetchTime: Date? {get}
    
    subscript(key: String) -> RemoteConfigValue? { get }
    
}

protocol RemoteConfigRepository: RemoteConfig {
    
    var requester: NetworkRequester {get}
    var remoteConfigStorage:RemoteConfigStorage {get}
    
    func fetchRemoteConfig(completion: @escaping (Result<Void, Error>) -> Void)
    
    func configValue(forKey key: String) -> RemoteConfigValue?
    func defaultValue(forKey key: String) -> RemoteConfigValue?
    
    func setConfigValueToRepo(forKey key: String, value: AnyObject?)
    func setDefaultValueToRepo(forKey key: String, value: AnyObject?)
    
    func setConfigValuesToRepo(fromPlist: String)
    func setDefaultValuesToRepo(fromPlist: String)
    
    func clearConfig(for type: RemoteConfigSource)

}

struct DefaultRemoteConfigRepository : RemoteConfigRepository, RemoteConfig {
    
    static let keyConfigExpired : String = "ConfigExpiredNotificationKey"
    
    class RemoteConfigValues {
        var lastFetchTime: Date?
        
        var configValues: [String:RemoteConfigValue] = [:]
        var localValues: [String:RemoteConfigValue] = [:]
        var defaultValues: [String:RemoteConfigValue] = [:]
    }
    
    var remoteConfigValues : RemoteConfigValues = RemoteConfigValues()
    
    var requester: NetworkRequester
    var remoteConfigStorage:RemoteConfigStorage
    var configObserver:RemoteConfigObserver
    var configSettings: RemoteConfigSettings = RemoteConfigSettings()
    
    var lastFetchTime: Date? {
        get {
            return (self.remoteConfigValues.lastFetchTime == nil) ?
                  self.remoteConfigStorage.getConfigFetchTime()
                : self.remoteConfigValues.lastFetchTime
        }
    }
    
    init(requester: NetworkRequester, remoteConfigStorage: RemoteConfigStorage, configObserver: RemoteConfigObserver) {
        self.requester = requester
        self.remoteConfigStorage = remoteConfigStorage
        self.configObserver = configObserver
    }
    
    subscript(key: String) -> RemoteConfigValue? {
        return self.configValue(forKey: key)
    }
    
    func fetchRemoteConfig(completion: @escaping (Result<Void, Error>) -> Void) {
        if(!isConfigExpired() && isConfigValuesCached()) {
            self.initCachedValues()
            completion(.success(()))
        } else {
            requester.request(with: APIEndpoints.getRemoteConfig(), completion: { (result) in
                switch result {
                case .success(let config):
                    print(config)
                    self.updateRemoteConfig(values: config)
                    completion(.success(()))
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        }
    }
    
    func saveValuesToCache(values: [String:AnyHashable]) {
        self.remoteConfigStorage.setConfigFetchTime(date: self.lastFetchTime!)
        self.remoteConfigStorage.setConfigValues(values: values)
        if self.configSettings.useAutoFetch {
            self.configObserver.startObserving()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(self.configSettings.minimumFetchInterval)) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: DefaultRemoteConfigRepository.keyConfigExpired), object: nil, userInfo: nil)
            }
        }
    }
    
    func initCachedValues() {
        self.remoteConfigValues.configValues.removeAll()
        let values = self.remoteConfigStorage.getConfigValues() ?? [:]
        for (key, value) in values {
            let value = value as AnyObject
            self.remoteConfigValues.configValues[key] = RemoteConfigValue(value: value, source: .remote)
        }
        if self.configSettings.useAutoFetch {
            self.configObserver.startObserving()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(self.configSettings.minimumFetchInterval)) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: DefaultRemoteConfigRepository.keyConfigExpired), object: nil, userInfo: nil)
            }
        }
    }
    
    func updateRemoteConfig(values: [String:AnyHashable]) {
        self.remoteConfigValues.lastFetchTime = Date()
        self.remoteConfigValues.configValues.removeAll()

        for (key, value) in values {
            let value = value as AnyObject
            self.remoteConfigValues.configValues[key] = RemoteConfigValue(value: value, source: .remote)
        }
        
        self.saveValuesToCache(values: values)
    }
    
    func configValue(forKey key: String) -> RemoteConfigValue? {
        if let value = self.remoteConfigValues.configValues[key] {
            return value
        }
        if let value = self.remoteConfigValues.localValues[key] {
            return value
        }
        if let value = self.remoteConfigValues.defaultValues[key] {
            return value
        }
        return nil
    }
    
    func defaultValue(forKey key: String) -> RemoteConfigValue? {
        if let value = self.remoteConfigValues.defaultValues[key] {
            return value
        }
        return nil
    }
    
    func setConfigValueToRepo(forKey key: String, value: AnyObject?) {
        if let value = value {
            self.remoteConfigValues.localValues[key] = RemoteConfigValue(value: value, source: .local)
            self.remoteConfigValues.configValues.removeValue(forKey: key)
        } else {
            self.remoteConfigValues.localValues.removeValue(forKey: key)
            self.remoteConfigValues.configValues.removeValue(forKey: key)
        }
    }
    
    func setDefaultValueToRepo(forKey key: String, value: AnyObject?) {
        if let value = value {
            self.remoteConfigValues.defaultValues[key] = RemoteConfigValue(value: value, source: .default)
        } else {
            self.remoteConfigValues.defaultValues.removeValue(forKey: key)
        }
    }
    
    func setConfigValuesToRepo(fromPlist: String) {
        self.clearConfig(for: .local)
        var defaultDict: [String:AnyObject?]?
        if let path = Bundle.main.path(forResource: fromPlist, ofType: "plist") {
            defaultDict = NSDictionary(contentsOfFile: path) as? [String : AnyObject?]
        }
        if let dict = defaultDict {
            for (key, value) in dict {
                self.setConfigValueToRepo(forKey: key, value: value)
            }
        }
    }
    
    func setDefaultValuesToRepo(fromPlist: String) {
        self.clearConfig(for: .default)
        var defaultDict: [String:AnyObject?]?
        if let path = Bundle.main.path(forResource: fromPlist, ofType: "plist") {
            defaultDict = NSDictionary(contentsOfFile: path) as? [String : AnyObject?]
        }
        if let dict = defaultDict {
            for (key, value) in dict {
                self.setDefaultValueToRepo(forKey: key, value: value)
            }
        }
    }
    
    func clearConfig(for type: RemoteConfigSource) {
        switch type {
        case .remote:
            self.remoteConfigValues.lastFetchTime = nil
            self.remoteConfigValues.configValues.removeAll()
            self.remoteConfigStorage.clearConfigSettings()
        case .local:
            self.remoteConfigValues.localValues.removeAll()
        case .default:
            self.remoteConfigValues.defaultValues.removeAll()
        case .all:
            self.remoteConfigValues.lastFetchTime = nil
            self.remoteConfigValues.configValues.removeAll()
            self.remoteConfigValues.localValues.removeAll()
            self.remoteConfigValues.defaultValues.removeAll()
            self.remoteConfigStorage.clearConfigSettings()
        }
    }

    func isConfigExpired() -> Bool {
        guard let fetchDate = self.lastFetchTime else {
            return true
        }
        let dateNow = Date()
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([Calendar.Component.second], from: fetchDate, to: dateNow)
        let seconds = dateComponents.second
        
        return seconds ?? 0 > self.configSettings.minimumFetchInterval
    }
    
    func isConfigValuesCached() -> Bool {
        return (self.remoteConfigStorage.getConfigValues() == nil) ? false : true
    }
    

    
}

