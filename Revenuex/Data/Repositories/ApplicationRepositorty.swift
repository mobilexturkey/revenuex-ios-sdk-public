//
//  ApplicationRepositorty.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 5.05.2021.
//

import Foundation

protocol ApplicationRepositorty {
    var isFirstLaunch:Bool {get}
    
    func setRevenuexId(_ value:String)
    func getRevenuexId() -> String?
    func setReceiptString(receipt: String)
    func getReceiptString() -> String?
    
}

struct DefaultApplicationRepositorty : ApplicationRepositorty {
    
    var userDefaults:UserDefaults
    
    var isFirstLaunch: Bool {
        guard let _ = userDefaults.value(forKey: "isFirstLaunch") as? Bool else {
            userDefaults.setValue(true, forKey: "isFirstLaunch")
            return true
        }
        
        return false
    }
    
    func setRevenuexId(_ value: String) {
        userDefaults.setValue(value, forKey: "revenuexId")
    }
    
    func getRevenuexId() -> String? {
        return userDefaults.string(forKey: "revenuexId")
    }
    
    func setReceiptString(receipt: String) {
        userDefaults.setValue(receipt, forKey: "RevX_ReceiptString")
    }
    
    func getReceiptString() -> String? {
        return userDefaults.object(forKey: "RevX_ReceiptString") as? String
    }

}
