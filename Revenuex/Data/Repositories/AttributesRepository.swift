//
//  AttributesRepository.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 5.05.2021.
//

import Foundation

protocol AttributesRepository {
    
    var requester: NetworkRequester {get}
    var cache:AttributesStorage {get}
    
    func getUnsyncedAttributes(completion:@escaping (Result<[AttributeDTO],Error>) -> Void)
    func setAttributeLocally(attribute: AttributeDTO,isSyncedWithServer: Bool, completion: @escaping EmptyResult)
    func getAttribute(with key:String, completion: @escaping (Result<AttributeDTO?, Error>) -> Void)
    func syncAttributes(attributes:[AttributeDTO], completion: @escaping EmptyResult)

}

struct DefaultAttributesRepository : AttributesRepository {
    
    var requester: NetworkRequester
    var cache:AttributesStorage
    
    func setAttributeLocally(attribute: AttributeDTO,isSyncedWithServer: Bool, completion: @escaping EmptyResult) {
        cache.setAttiribute(attribute: attribute, isSyncedWithServer: isSyncedWithServer, completion: completion)
    }
    
    
    func syncAttributes(attributes:[AttributeDTO], completion: @escaping EmptyResult) {
        do {
            
            let attributesMap = try attributes.compactMap{
                return try $0.toDictionary()
            }
            
            requester.request(with: APIEndpoints.syncAttirbutes(attiribues: attributesMap)) { (result) in
                switch result {
                case .success:
                    attributes.forEach({
                        setAttributeLocally(attribute: $0, isSyncedWithServer: true, completion: completion)
                    })
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
        } catch  {
            completion(.failure(error))
        }
    }
    
    func getUnsyncedAttributes(completion: @escaping (Result<[AttributeDTO], Error>) -> Void) {
        cache.getUnsyncedAttributes(completion: completion)
    }
    
    func getAttribute(with key:String, completion: @escaping (Result<AttributeDTO?, Error>) -> Void) {
        cache.getAttribute(with: key, completion: completion)
    }

    
}
