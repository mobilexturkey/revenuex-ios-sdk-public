//
//  TransactionsRepository.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 27.04.2021.
//

import Foundation
import StoreKit

protocol PurchasesRepository {
    
    var requester: NetworkRequester {get}
    
    func sendPaymentInfo(product: RevenueXProduct,
                         completion: EmptyResult?)
    
    func queryProductsFromRemote(completion: @escaping (Result<[RevenueXProduct],Error>) -> Void)
    
    func fetchExternalProductsFromApple(products: Set<String>, completion: @escaping (Result<[RevenueXProduct], Error>) -> Void)
    
    func purchaseRevenueXProduct(product: RevenueXProduct, completion: @escaping ((Result<RevenueXProduct, Error>) -> Void)) -> Void
    
    func sendReceiptData(completion: @escaping ((Result<Void, Error>) -> Void)) -> Void
    
    func canMakePayment() -> Bool
    
}

protocol PurchaseRepositoryIAPProtocol {
    var products: [RevenueXProduct] {get set}
}

struct DefaultPurchasesRepository : PurchasesRepository, PurchaseRepositoryIAPProtocol {
    
    var products: [RevenueXProduct] = []
    var requester: NetworkRequester
    
    let iapHelper: IAPHelper = IAPHelper()

    func queryProductsFromRemote(completion: @escaping (Result<[RevenueXProduct], Error>) -> Void) {
        if self.products.count > 0 {
            completion(.success(products))
        }
        iapHelper.productRequestCompletionBlock = completion
        requester.request(with: APIEndpoints.getProductsFromRemote()) { (result) in
            switch result {
            case .success(let result):
                var productIds: Set<String> = []
                for item in result {
                    if let product = item.toRevXProduct() {
                        productIds.insert(product.identifier)
                    }
                }
                self.fetchProductsFromApple(products: productIds)
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
    func purchaseRevenueXProduct(product: RevenueXProduct, completion: @escaping ((Result<RevenueXProduct, Error>) -> Void)) -> Void {
        iapHelper.productPurchaseCompletionBlock = completion
        iapHelper.purchaseDelegate = nil
        iapHelper.buyProduct(product)
    }
    
    func fetchProductsFromApple(products: Set<String>) {
        iapHelper.productIdentifiers = products
        iapHelper.purchaseDelegate = self
        iapHelper.requestProducts()
    }
    
    func fetchExternalProductsFromApple(products: Set<String>, completion: @escaping (Result<[RevenueXProduct], Error>) -> Void) {
        iapHelper.productRequestCompletionBlock = completion
        iapHelper.productIdentifiers = products
        iapHelper.purchaseDelegate = nil
        iapHelper.productPurchaseCompletionBlock = nil
        iapHelper.requestProducts()
    }
    
    func sendPaymentInfo(product: RevenueXProduct,
                         completion: EmptyResult?) {
        guard let receiptURL = Bundle.main.appStoreReceiptURL, let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString() else {
                return
        }
        requester.request(with: APIEndpoints.sendPaymentInfo(receiptData: receiptString,
                                                             price: product.price,
                                                             productId: product.productIdentifier,
                                                             currency: product.currency,
                                                             period: product.period,
                                                             isSubscription: product.isSub)) { (result) in
            switch result {
            case .success():
                completion?(.success(()))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func sendReceiptData(completion: @escaping ((Result<Void, Error>) -> Void)) -> Void {
        guard let receiptURL = Bundle.main.appStoreReceiptURL, let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString() else {
                return
        }
        
        requester.request(with: APIEndpoints.sendReceiptData(receiptData: receiptString,
                                                             isSubscription: false)) { (result) in
            switch result {
            case .success():
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func canMakePayment() -> Bool {
        return iapHelper.canMakePayments()
    }
}

