//
//  UserRepository.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 19.04.2021.
//

import Foundation

protocol RevenuexUserRepository {
    
    var requester: NetworkRequester {get}
    var cache:RevenuexUserStorage {get}
    
    func sendOpenEvent(completion: EmptyResult?)

}

struct DefaultRevenuexUserRepository : RevenuexUserRepository {
    
    var requester: NetworkRequester
    var cache:RevenuexUserStorage
    
    func sendOpenEvent(completion: EmptyResult?) {
        requester.request(with: APIEndpoints.sendOpenEvent()) { (result) in
            switch result {
            case .success(let user):
                cache.createOrUpdateRevenuexUser(user: user, completion: {completion?($0)})
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
}
