//
//  AttributesStorage.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 5.05.2021.
//

import Foundation

protocol AttributesStorage {
    func getUnsyncedAttributes(completion: @escaping (Result<[AttributeDTO], Error>) -> Void)
    func setAttiribute(attribute: AttributeDTO, isSyncedWithServer:Bool, completion: @escaping (Result<Void, Error>) -> Void)
    func getAttribute(with key:String, completion: @escaping (Result<AttributeDTO?, Error>) -> Void)
}
