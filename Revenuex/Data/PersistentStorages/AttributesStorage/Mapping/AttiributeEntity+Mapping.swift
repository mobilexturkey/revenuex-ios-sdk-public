//
//  AttiributeEntity+Mapping.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 4.05.2021.
//

import Foundation
import CoreData

extension AttributeEntity {
    func toDTO() -> AttributeDTO {
        return .init(key: key ?? "unknownKey", value: value ?? "unknownValue")
    }
}

extension AttributeDTO {
    func toEntity(in context: NSManagedObjectContext) -> AttributeEntity {
        let entity = AttributeEntity(context: context)
        entity.key = key
        entity.value = value
        return entity
    }
}
