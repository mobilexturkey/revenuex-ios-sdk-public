//
//  CoreDataAttributesStorage.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 5.05.2021.
//

import Foundation
import CoreData

struct CoreDataAttributesStorage : AttributesStorage {
    
    var coreDataStorage: CoreDataStorage
    
    func getUnsyncedAttributes(completion: @escaping (Result<[AttributeDTO], Error>) -> Void) {
        let fetchRequest: NSFetchRequest = AttributeEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "synced == %d", false)
        
        coreDataStorage.performBackgroundTask { context in
            do {
                let result = try context.fetch(fetchRequest)
                completion(.success(result.map({$0.toDTO()})))
            } catch {
                completion(.failure(error))
            }
             
        }
    }
    
    func getAttribute(with key:String, completion: @escaping (Result<AttributeDTO?, Error>) -> Void) {
        let fetchRequest: NSFetchRequest = AttributeEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "key == %@", key)
        
        coreDataStorage.performBackgroundTask { context in
            do {
                let result = try context.fetch(fetchRequest).first
                completion(.success(result?.toDTO()))
            } catch {
                completion(.failure(error))
            }
             
        }
    }
    
    func setAttiribute(attribute: AttributeDTO, isSyncedWithServer:Bool, completion: @escaping (Result<Void, Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            do {
                let entity = attribute.toEntity(in: context)
                entity.synced = isSyncedWithServer
                context.insert(entity)
                try context.save()
                completion(.success(()))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
