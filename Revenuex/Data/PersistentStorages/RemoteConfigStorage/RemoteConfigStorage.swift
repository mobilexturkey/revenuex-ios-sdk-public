//
//  RemoteConfigStorage.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 6.07.2021.
//

import Foundation

protocol RemoteConfigStorage {

    func setConfigFetchTime(date: Date)
    func getConfigFetchTime() -> Date?
    func setConfigValues(values: [String:AnyHashable])
    func getConfigValues() -> [String:AnyHashable]?
    func clearConfigSettings()
    
}

struct DefaultRemoteConfigStorage : RemoteConfigStorage {
    
    var userDefaults:UserDefaults
    
    func setConfigFetchTime(date: Date) {
        userDefaults.setValue(date, forKey: "RevX_ConfigFetchTime")
    }
    
    func getConfigFetchTime() -> Date? {
        return userDefaults.object(forKey: "RevX_ConfigFetchTime") as? Date
    }
    
    func setConfigValues(values: [String:AnyHashable]) {
        userDefaults.setValue(values, forKey: "RevX_ConfigValues")
    }
    
    func getConfigValues() -> [String:AnyHashable]? {
        return userDefaults.object(forKey: "RevX_ConfigValues") as? [String:AnyHashable]
    }
    
    func clearConfigSettings()  {
        userDefaults.removeObject(forKey: "RevX_ConfigValues")
        userDefaults.removeObject(forKey: "RevX_ConfigFetchTime")
    }

}
