//
//  RevenuexUserEntity+Mapping.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 4.05.2021.
//

import Foundation
import CoreData

extension CurrentRevenuexUserEntity {
    func toDTO() -> CurrentRevenuexUserDTO {
        return .init(applicationId: applicationId,
                     revenuexId: revenuexId,
                     platformVersion: platformVersion)
    }
}

extension CurrentRevenuexUserDTO {
    func toEntity(in context: NSManagedObjectContext) -> CurrentRevenuexUserEntity {
        let entity = CurrentRevenuexUserEntity(context: context)
        entity.applicationId = applicationId
        entity.revenuexId = revenuexId
        entity.platformVersion = platformVersion
        return entity
    }
}
