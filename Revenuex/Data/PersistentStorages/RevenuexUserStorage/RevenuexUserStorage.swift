//
//  UserStorageProtocol.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 19.04.2021.
//

import Foundation

protocol RevenuexUserStorage {
    func createOrUpdateRevenuexUser(user: CurrentRevenuexUserDTO, completion: @escaping (Result<Void, Error>) -> Void)
}
