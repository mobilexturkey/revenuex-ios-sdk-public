//
//  CoreDataRevenuexUserStorage.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 3.05.2021.
//

import Foundation
import CoreData

struct CoreDataRevenueXUserStorage : RevenuexUserStorage {

    var coreDataStorage: CoreDataStorage
    
    func createOrUpdateRevenuexUser(user: CurrentRevenuexUserDTO, completion: @escaping (Result<Void, Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            do {
                context.insert(user.toEntity(in: context))
//                context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
                try context.save()
                completion(.success(()))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
}
