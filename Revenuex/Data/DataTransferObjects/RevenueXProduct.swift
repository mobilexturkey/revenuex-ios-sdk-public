//
//  OfferingDTO.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 29.04.2021.
//

import Foundation
import StoreKit


//public typealias  = ProductDTO

public struct RevenueXProduct  {
//    var userId:String?
    
    public var productIdentifier:String {
        get {return product?.productIdentifier ?? ""}
    }
    
    public var price:Double {
        get {return Double(product!.price.floatValue)}
    }
    
    public var currency:String {
        get {return product?.priceLocale.currencyCode ?? ""}
    }
    
    public var period:String {
        get {
            if #available(iOS 11.2, *) {
                //weekly, monthly, twoMonth, threeMonth, sixMonth, yearly
                switch product?.subscriptionPeriod?.unit {
                case .day:
                    if (product?.subscriptionPeriod?.numberOfUnits ?? 0) == 7 {
                        return "weekly"
                    }
                    return "daily"
                case .week:
                    return "weekly"
                case .month:
                    switch product?.subscriptionPeriod?.numberOfUnits {
                    case 1:
                        return "monthly"
                    case 2:
                        return "twoMonth"
                    case 3:
                        return "threeMonth"
                    case 6:
                        return "sixMonth"
                    default:
                        return ""
                    }
                case .year:
                    return "yearly"
                default:
                    return ""
                }
            } else {
                return ""
            }
        }
    }
    
    public var localizedTitle:String {
        get {return ((product?.localizedTitle.count ?? 0) > 0 ?
                        (product?.localizedTitle)!
                        :   "AutoRenew - \(self.period)") }
    }
    
    public var product:SKProduct?
    
    var isSub:Bool {
        get {return self.period.count > 0}
    }
    
    var identifier: String
    
    init(_identifier: String,_ product: SKProduct? = nil) {
        self.identifier = _identifier
        self.product = product
    }
    
}

class ProductResponseModel: Codable {
    
    var productId: String
    var platform: String
    
    enum CodingKeys: String, CodingKey {
            case productId = "productId"
            case platform = "platform"
    }
    
    enum PlatformKeys: String {
            case iOS = "ios"
            case android = "android"
    }

    func toRevXProduct() -> RevenueXProduct? {
        switch self.platform {
        case PlatformKeys.iOS.rawValue:
            return RevenueXProduct(_identifier: self.productId)
        default:
            return nil
        }
    }
}

