//
//  ApplicationUserDTO.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 19.04.2021.
//

import Foundation

struct CurrentRevenuexUserDTO : Codable{
    var applicationId: String?
    var revenuexId: String?
    var platformVersion: String?
    
    enum CodingKeys: String, CodingKey {
            case applicationId = "applicationId"
            case revenuexId = "revenueXId"
            case platformVersion
    }
    
}
