//
//  AttiributeDTO.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 3.05.2021.
//

import Foundation

struct AttributeDTO : Codable {
    var key: String
    var value: String
}
