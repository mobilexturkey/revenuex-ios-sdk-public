//
//  RevenueXError.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 13.07.2021.
//

import Foundation


class RevenueXError : Error {
    
    var errorCode: Int {
        didSet {
            switch errorCode {
            case 0:
                break
            case 401:
                self.errorType = .serverError
                self.serverErrorType = .invalidClientIDError
            case 400..<500:
                self.errorType = .serverError
                self.serverErrorType = .badRequestError
            default:
                self.errorType = .serverError
                self.serverErrorType = .externalServerError
            }
        }
    }
    
    var errorType: ErrorType?
    var serverErrorType: ServerErrorType?
    
    var title: String {
        get {
            return "\(self.errorType?.enumDetails ?? "") - \(self.serverErrorType?.enumDetails ?? "")"
        }
    }
    
    init(with error: Error, errorCode: Int) {
        self.errorCode = errorCode
    }
    
}


enum ErrorType : Int {
    case serverError
    case internalSDKError
    case networkError
    case invalidParametersError
    
    public var enumDetails: String {
        switch self {
        case .serverError: return "Server error"
        case .internalSDKError: return "SDK internal error"
        case .networkError: return "Network error"
        case .invalidParametersError: return "Invalid parameters error"
        }
    }
}

enum ServerErrorType : Int {
    case parseJSONError
    case badRequestError
    case externalServerError
    case invalidClientIDError
    
    public var enumDetails: String {
        switch self {
        case .parseJSONError: return "Error while parsing JSON"
        case .badRequestError: return "Bad request"
        case .externalServerError: return "Server error"
        case .invalidClientIDError: return "Client ID invalid"
        }
    }
}


enum RequestServiceError: Error {
    case noResponse
    case parsing(Error)
    case networkFailure(NetworkError)
    case resolvedNetworkFailure(Error)
}

enum NetworkError: Error {
    case error(statusCode: Int, data: Data?)
    case notConnected
    case cancelled
    case generic(Error)
    case urlGeneration
}
