//
//  RemoteConfigObserver.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 28.07.2021.
//

import Foundation

protocol RemoteConfigObserver {
    func startObserving()
    func stopObserving()
}

protocol RemoteConfigObserverDelegate {
    func configExpired()
}

class DefaultRemoteConfigObserver : RemoteConfigObserver {
    
    var delegate: RemoteConfigObserverDelegate?
    
    func startObserving() {
        self.stopObserving()
        NotificationCenter
            .default
            .addObserver(self, selector: #selector(configExpired(_:)), name: NSNotification.Name(DefaultRemoteConfigRepository.keyConfigExpired), object: nil)
    }
    
    func stopObserving() {
        NotificationCenter
            .default
            .removeObserver(self)
    }
    
    deinit {
        NotificationCenter
            .default
            .removeObserver(self)
    }
    
}

private extension DefaultRemoteConfigObserver {
    
    @objc func configExpired(_ notification : NSNotification) {
        delegate?.configExpired()
    }
    
}
