//
//  PaymentObserver.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 27.04.2021.
//

import Foundation
import StoreKit

protocol PaymentObserver {
    var paymentQueue:SKPaymentQueue {get}
    var delegate:PaymentObserverDelegate? {get set}
    
    func handlePurchasedTransaction(transactions: [SKPaymentTransaction])
    func startObserving()
    
}

protocol PaymentObserverDelegate {
    func paymentObserverDidHandlePurchasedTransaction(transactions: [SKPaymentTransaction])
}

class DefaultPaymentObserver : NSObject, PaymentObserver {

    var paymentQueue: SKPaymentQueue
    var delegate:PaymentObserverDelegate?
    
    init(paymentQueue: SKPaymentQueue) {
        self.paymentQueue = paymentQueue
        super.init()
    }
    
    func handlePurchasedTransaction(transactions: [SKPaymentTransaction]) {
        self.delegate?.paymentObserverDidHandlePurchasedTransaction(transactions: transactions)

    }
    
    func startObserving() {
        self.paymentQueue.add(self)
    }
    
}

extension DefaultPaymentObserver : SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        let purchasedTransactions = transactions.filter({ $0.transactionState == .purchased })
        if purchasedTransactions.count > 0 {
            handlePurchasedTransaction(transactions: transactions)
        }
    }
}
