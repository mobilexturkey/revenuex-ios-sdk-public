//
//  IAPHelper.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 23.06.2021.
//

import Foundation
import StoreKit

public typealias ProductIdentifier = String


open class IAPHelper: NSObject  {
  
    var productIdentifiers: Set<ProductIdentifier> = []
    var product: RevenueXProduct?
    var purchaseDelegate: PurchaseRepositoryIAPProtocol?
    
    var productRequestCompletionBlock : ((Result<[RevenueXProduct], Error>) -> Void)?
    var productPurchaseCompletionBlock : ((Result<RevenueXProduct, Error>) -> Void)?
    
    private var productsRequest: SKProductsRequest?
  
    public init(productIds: Set<ProductIdentifier> = []) {
        productIdentifiers = productIds
        super.init()

        SKPaymentQueue.default().add(self)
      }
    
    public func requestProducts() {
        productsRequest?.cancel()
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self
        productsRequest!.start()
    }
    
    public func buyProduct(_ product: RevenueXProduct) {
        print("Buying \(product.productIdentifier)...")
        self.product = product
        let payment = SKPayment(product: product.product!)
        SKPaymentQueue.default().add(payment)
    }

    public func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    public func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

// MARK: - SKProductsRequestDelegate

extension IAPHelper: SKProductsRequestDelegate {

    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded list of products...")
        let products = response.products


        var productsArr : [RevenueXProduct] = []
        
        for p in products {
            let item = RevenueXProduct(_identifier: p.productIdentifier , p)
            productsArr.append(item)
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
        
        self.purchaseDelegate?.products = productsArr
        self.productRequestCompletionBlock?(.success(productsArr))
        
        clearRequestAndHandler()
    }

    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        self.productRequestCompletionBlock?(.failure(error))
        clearRequestAndHandler()
    }

    private func clearRequestAndHandler() {
        productsRequest = nil
    }
}


// MARK: - SKPaymentTransactionObserver

extension IAPHelper: SKPaymentTransactionObserver {

    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            @unknown default:
                break
            }
        }
  }

    private func complete(transaction: SKPaymentTransaction) {
        print("complete...")
        deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
        self.productPurchaseCompletionBlock?(.success(self.product!))
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }

        print("restore... \(productIdentifier)")
        deliverPurchaseNotificationFor(identifier: productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as NSError?,
           let localizedDescription = transaction.error?.localizedDescription,
           transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }
        self.productPurchaseCompletionBlock?(.failure(transaction.error!))
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    private func deliverPurchaseNotificationFor(identifier: String?) {
//        guard let identifier = identifier else { return }
//
//        purchasedProductIdentifiers.insert(identifier)
//        UserDefaults.standard.set(true, forKey: identifier)
//        NotificationCenter.default.post(name: .IAPHelperPurchaseNotification, object: identifier)
    }
}



