//
//  RemoteConfigSettings.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 6.07.2021.
//

import Foundation

public class RemoteConfigSettings {
    
    public static let notificationKeyConfigRefreshed : String = "notificationKeyConfigRefreshed"
    
    var _minimumFetchInterval: Int = 60
    public var minimumFetchInterval: Int {
        get {
            return self._minimumFetchInterval
//            #if DEBUG
//            return self._minimumFetchInterval
//            #else
//            if(self._minimumFetchInterval < (3*60*60)) {
//                self._minimumFetchInterval = 3*60*60
//            }
//            return self._minimumFetchInterval
//            #endif
        }
        set {
            self._minimumFetchInterval = newValue
        }
    }

    public var fetchTimeout: TimeInterval?
    
    public var useAutoFetch: Bool = true
    
    public init() {}
    
}
