//
//  RemoteConfigValue.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 6.07.2021.
//

import Foundation

public class RemoteConfigValue {

    var value: AnyObject?
    
    init(value: AnyObject, source: RemoteConfigSource) {
        self.value = value
        self.source = source
    }
    
    /// Gets the value as a string.
    public var stringValue: String? {
        if let value = self.value {
            return value as? String
        }
        return nil
        
    }

    
    /// Gets the value as a number value.
    public var numberValue: NSNumber? {
        if let value = self.value {
            return value as? NSNumber
        }
        return nil
    }

    
    /// Gets the value as a NSData object.
    public var dataValue: Data? {
        if let value = self.value {
            return value as? Data
        }
        return nil
    }

    
    /// Gets the value as a boolean.
    public var boolValue: Bool {
        if let value = self.value {
            return (value as? Bool) ?? false
        }
        return false
    }
    
    /// Identifies the source of the fetched value.
    public var source: RemoteConfigSource

}


public enum RemoteConfigSource : Int {
    
    ///< The data source is the Remote Config service.
    case remote = 0

    ///< The data source is the DefaultConfig defined for this app.
    case `default` = 1

    ///< The data doesn't exist, return a static initialized value.
    case local = 2
    
    
    case all = 3
    
    public var enumDetails: String {
        switch self {
        case .remote: return "remote"
        case .default: return "default"
        case .local: return "local"
        case .all: return "all"
        }
    }
}
