//
//  Encodable.swift
//  Pods-revflix
//
//  Created by Orhan DALGARA on 5.05.2021.
//

import Foundation

extension Encodable {
    func toDictionary() throws -> [String: Any]? {
        let data = try JSONEncoder().encode(self)
        let josnData = try JSONSerialization.jsonObject(with: data)
        return josnData as? [String : Any]
    }
}
