//
//  ServiceConfig.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 19.04.2021.
//

import Foundation

public protocol NetworkConfiguration {
    var baseURL: URL { get }
    var headers: [String: String] { get set}
    var queryParameters: [String: String] { get }
}

struct DefaultNetworkConfiguration: NetworkConfiguration {
    let baseURL: URL = SystemInfo.baseURL
    var headers: [String: String] = [
        "platform":SystemInfo.platform,
        "platform-version":SystemInfo.platformVersion,
        "sandbox":"\(SystemInfo.isSandbox)",
        "app-version":SystemInfo.appVersion,
        "region":SystemInfo.region,
        "device-name":SystemInfo.deviceName,
        "Content-Type":"application/json"
    ]
    let queryParameters: [String: String] = [:]
}
