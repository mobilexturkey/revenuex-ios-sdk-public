//
//  TypeAliases.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 29.04.2021.
//

import Foundation

public typealias EmptyResult = ((Result<Void, Error>) -> Void)

