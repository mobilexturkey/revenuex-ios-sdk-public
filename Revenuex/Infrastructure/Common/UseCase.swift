//
//  UseCase.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 13.04.2021.
//

import Foundation


protocol UseCase {
    associatedtype ExecutionOutput
    associatedtype CompletionBlockOutput
    func execute(completion:((Result<CompletionBlockOutput,Error>) -> Void)?) -> ExecutionOutput
}

extension UseCase {
    func execute() -> ExecutionOutput {
        return execute(completion: nil)
    }
}




