//
//  SystemInfo.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 17.04.2021.
//

import Foundation
import AppTrackingTransparency
import AdSupport
import UIKit

struct SystemInfo {
    
    //TODO: Fix dummy values below. (Level-1)
    
    static var baseURL:URL {

        let url = UserDefaults.standard.string(forKey: "revenuexIdURL") ?? ""
    
        if url != "" {
            return URL(string: url)!
        }
        return  URL(string: "https://sdk-test.revenueplus.net")!

    }
    
    static var platform: String {
        return UIDevice.current.systemName.lowercased()
    }
    
    static var platformVersion: String {
        return UIDevice.current.systemVersion
    }
    
    static var region: String {
        return Locale.current.regionCode ?? ""
    }
    
    static var appVersion: String {
        return readFromInfoPlist(withKey: "CFBundleShortVersionString") ?? ""
    }
    
    static var isSandbox: Bool {
        #if DEBUG
            return true
        #elseif ADHOC
            return true
        #else
            return true
        #endif
    }
    
    static var deviceName: String {
        return UIDevice.current.name
    }
    
    static var IDFA: String? {
        if #available(iOS 14, *) {
            if ATTrackingManager.trackingAuthorizationStatus != ATTrackingManager.AuthorizationStatus.authorized  {
                return nil
            }
        } else {
            if ASIdentifierManager.shared().isAdvertisingTrackingEnabled == false {
                return nil
            }
        }

        return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }
    
    static var IDFV: String? {
        if let idfv = UIDevice.current.identifierForVendor?.uuidString {
            return idfv
        }
        
        return nil
    }
    
    static var IPAdress: String? {
        return getIPAddress()
    }
}

private extension SystemInfo {
    
    static let infoPlistDictionary = Bundle.main.infoDictionary
    
    static func getIPAddress() -> String? {
       var address: String?
       var ifaddr: UnsafeMutablePointer<ifaddrs>?
       if getifaddrs(&ifaddr) == 0 {
           var ptr = ifaddr
           while ptr != nil {
               defer { ptr = ptr?.pointee.ifa_next }
               let interface = ptr?.pointee
               let addrFamily = interface?.ifa_addr.pointee.sa_family
               if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6),
                   let cString = interface?.ifa_name,
                   String(cString: cString) == "en0",
                   let saLen = (interface?.ifa_addr.pointee.sa_len) {
                   var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                   let ifaAddr = interface?.ifa_addr
                   getnameinfo(ifaAddr,
                               socklen_t(saLen),
                               &hostname,
                               socklen_t(hostname.count),
                               nil,
                               socklen_t(0),
                               NI_NUMERICHOST)
                   address = String(cString: hostname)
               }
           }
           freeifaddrs(ifaddr)
       }
        
       return address
    }
    
    static func readFromInfoPlist(withKey key: String) -> String? {
        return infoPlistDictionary?[key] as? String
    }
}


