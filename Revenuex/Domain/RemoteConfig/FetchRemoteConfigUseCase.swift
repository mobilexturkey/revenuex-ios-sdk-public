//
//  FetchRemoteConfigUseCase.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 6.07.2021.
//

import Foundation

protocol FetchRemoteConfigUseCaseProtocol : UseCase {
    var remoteConfigRepository:RemoteConfigRepository {get}
}

struct FetchRemoteConfigUseCase : FetchRemoteConfigUseCaseProtocol {
    
    var remoteConfigRepository: RemoteConfigRepository
    
    func execute(completion: ((Result<Void, Error>) -> Void)?) -> Void {
        remoteConfigRepository.fetchRemoteConfig{ (result) in
            switch result {
            case .success(_):
                completion?(.success(()))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
        
    }

}
