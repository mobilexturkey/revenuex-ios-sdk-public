//
//  GetCurrentRevenuexUserId.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 4.05.2021.
//

import Foundation

protocol GetCurrentRevenuexUserIdProtocol : UseCase {
    var applicationRepository : ApplicationRepositorty {get}
}

struct GetRevenuexIdUseCase : GetCurrentRevenuexUserIdProtocol {
    
    var applicationRepository: ApplicationRepositorty
    
    typealias ExecutionOutput = String
    
    typealias CompletionBlockOutput = Void
    
    func execute(completion: ((Result<Void, Error>) -> Void)?) -> String {
        return revenuexId
    }
    
    private var revenuexId:String {
        guard let revenuexId = applicationRepository.getRevenuexId() else {
            let userId = UUID.init().uuidString.replacingOccurrences(of: "-", with: "")
            applicationRepository.setRevenuexId(userId)
            return userId
        }
        return revenuexId
    }
}
