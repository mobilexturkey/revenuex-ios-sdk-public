//
//  File.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 30.04.2021.
//

import Foundation

protocol UpdateLastOpenUseCaseProtocol : UseCase {
    var userRepository:RevenuexUserRepository {get}
}

struct UpdateLastOpenUseCase : UpdateLastOpenUseCaseProtocol {
    
    typealias ExecutionOutput = Void
    
    typealias CompletionBlockOutput = Void
    
    var userRepository: RevenuexUserRepository
    
    func execute(completion: ((Result<Void, Error>) -> Void)?) -> Void {
        userRepository.sendOpenEvent(completion: completion)
    }


}
