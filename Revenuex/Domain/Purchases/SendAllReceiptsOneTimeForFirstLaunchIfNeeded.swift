//
//  SendAllReceiptsOneTimeForFirsLaunchIfNeeded.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 6.05.2021.
//

import Foundation

protocol SendAllReceiptsOneTimeForFirstLaunchIfNeededUseCaseProtocol : UseCase {
    var applicationRepository:ApplicationRepositorty {get}
    var purchasesRepository:PurchasesRepository {get}
}

struct SendAllReceiptsOneTimeForFirstLaunchIfNeededUseCase : SendAllReceiptsOneTimeForFirstLaunchIfNeededUseCaseProtocol {
    typealias ExecutionOutput = Void
    
    typealias CompletionBlockOutput = Void
    
    var applicationRepository: ApplicationRepositorty
    
    var purchasesRepository: PurchasesRepository
    
    func execute(completion: ((Result<Void, Error>) -> Void)?) -> Void {
        guard let receiptURL = Bundle.main.appStoreReceiptURL, let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString() else {
                return
        }
        if let cachedReceipt = applicationRepository.getReceiptString() {
            if receiptString == cachedReceipt {
                return
            }
        }
        
        purchasesRepository.sendReceiptData(completion: { (result) in
            switch result {
            case .success(_):
                self.applicationRepository.setReceiptString(receipt: receiptString)
                completion?(.success(()))
            case .failure(let error):
                completion?(.failure(error))
            }
        })
    }
    
}
