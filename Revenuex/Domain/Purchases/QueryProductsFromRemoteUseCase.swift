//
//  GetOfferingsUseCase.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 2.05.2021.
//

import Foundation

protocol QueryProductsFromRemoteUseCaseProtocol : UseCase {
    var purchasesRepository: PurchasesRepository {get}
}

struct QueryProductsFromRemoteUseCase : QueryProductsFromRemoteUseCaseProtocol {

    typealias ExecutionOutput = Void
    
    typealias CompletionBlockOutput = [RevenueXProduct]
    
    var purchasesRepository: PurchasesRepository
    
    func execute(completion: ((Result<[RevenueXProduct], Error>) -> Void)?) -> Void {
        purchasesRepository.queryProductsFromRemote { (result) in
            switch result {
            case .success(let offerings):
                completion?(.success(offerings))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }


}
