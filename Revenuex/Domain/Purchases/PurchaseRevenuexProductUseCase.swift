//
//  PurchaseRevenuexProductUseCase.swift
//  Revenuex
//
//  Created by Dogus Yigit Ozcelik on 29.06.2021.
//

import Foundation

protocol PurchaseRevenuexProductUseCaseProtocol : UseCase {
    var purchasesRepository: PurchasesRepository {get}
}

struct PurchaseRevenuexProductUseCase : PurchaseRevenuexProductUseCaseProtocol {

    typealias ExecutionOutput = Void
    
    typealias CompletionBlockOutput = RevenueXProduct
    
    var purchasesRepository: PurchasesRepository
    
    var product: RevenueXProduct
    
    
    func execute(completion: ((Result<RevenueXProduct, Error>) -> Void)?) -> Void {
        purchasesRepository.purchaseRevenueXProduct(product: product, completion: { (result) in
            switch result {
            case .success(let product):
                completion?(.success(product))
            case .failure(let error):
                completion?(.failure(error))
            }
        })
    }


}
