//
//  SyncAttiributionsUseCase.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 2.05.2021.
//

import Foundation

protocol SyncAttributesUseCaseProtocol : UseCase {
    var attributesRepository:AttributesRepository {get}
}

struct SyncAttributesUseCase : SyncAttributesUseCaseProtocol {
    
    typealias ExecutionOutput = Void
    
    typealias CompletionBlockOutput = Void
    
    var attributesRepository: AttributesRepository
    
    func execute(completion: ((Result<Void, Error>) -> Void)?) -> Void {
        attributesRepository.getUnsyncedAttributes { (result) in
            switch result {
            case .success(let attributes):
                guard attributes.isEmpty == false else {
                    completion?(.success(()))
                    return
                }
                
                attributesRepository.syncAttributes(attributes: attributes) { (result) in
                    switch result {
                    case .success():
                        completion?(.success(()))
                    case .failure(let error):
                        completion?(.failure(error))
                    }
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
        
    }

}
