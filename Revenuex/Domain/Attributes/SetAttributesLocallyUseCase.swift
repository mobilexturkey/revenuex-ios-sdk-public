//
//  SetAttiributionUseCase.swift
//  Revenuex
//
//  Created by Orhan DALGARA on 2.05.2021.
//

import Foundation

protocol SetAttiributionLocallyUseCaseProtocol : UseCase {
    var attributes:[AttributeDTO] {get}
    var attributesRepository:AttributesRepository {get}
}



struct SetAttributesLocallyUseCase : SetAttiributionLocallyUseCaseProtocol {
    
    
    
    typealias ExecutionOutput = Void
    
    typealias CompletionBlockOutput = Void
    
    var attributes: [AttributeDTO]
    
    var attributesRepository: AttributesRepository
    
    func execute(completion: ((Result<Void, Error>) -> Void)?) -> Void {
        attributes.forEach { (attribute) in
            attributesRepository.getAttribute(with: attribute.key) { (result) in
                switch result {
                case .success(let currentAttribute):
                    if currentAttribute == nil || currentAttribute?.value != attribute.value {
                        attributesRepository.setAttributeLocally(attribute: .init(key: attribute.key, value: attribute.value),
                                                            isSyncedWithServer: false,
                                                            completion: { result in
                                                                switch result {
                                                                case .success():
                                                                    completion?(.success(()))
                                                                case .failure(let error):
                                                                    completion?(.failure(error))
                                                                }
                                                            })
                    }
                    completion?(.success(()))
                case .failure(let error):
                    completion?(.failure(error))
                }
            }
        }
    }

}
