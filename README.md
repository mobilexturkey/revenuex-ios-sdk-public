# RevenueX iOS SDK

Instructions for implementing RevenueX SDK for iOS

- -  -

- [Requirements](#markdown-header-requirements)
- [Installation](#markdown-header-installation)
  	- [Install via CocoaPods](#markdown-header-install-via-cocoapods)
  	- [Install via Carthage](#markdown-header-install-via-carthage)
  	- [Install via Swift Package Manager](#markdown-header-install-via-swift-package-manager)
  	- [Importing RevenueX](#markdown-header-importing-revenuex)
- [Configuring the SDK](#markdown-header-configuring-the-sdk)
- [In App Purchases](#markdown-header-in-app-purchases)
  	- [Requesting Products](#markdown-header-requesting-products)
    - [Purchase Product](#markdown-header-purchase-product)
    - [Checking If Purchases Are Available](#markdown-header-checking-if-purchases-are-available)
- [Remoto Config](#markdown-header-remote-config)
    - [Fetching Remote Config](#markdown-header-fetching-remote-config)
    - [Auto Fetch](#markdown-header-auto-fetch)
    - [Default Values](#markdown-header-default-values)
    - [Local Source](#markdown-header-local-source)
- [App User](#markdown-header-app-user)



## Requirements


Xcode 12.5+

Minimum target: iOS 10.0+

- -  -


## Installation


RevenueX for iOS can be installed either via CocoaPods, Carthage, or Swift Package Manager.


- -  -
### Install via CocoaPods


Will be added after beta relase.


- -  -
### Install via Carthage


Will be added after beta relase.

- -  -

### Install via Swift Package Manager


Will be added after beta relase.


- -  -

### Importing RevenueX


In order to import Revenuex SDK you should add following statemetments to your project.


	import RevenueX


- -  -

## Configuring the SDK

Once you install the SDK, you need to configure it when your application finishes launching.

For configuration, you need to use your APIKey, which can be optain from web panel.

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
        Revenuex.shared.configure(with: "APIKey")
        
    }

- -  -

## In App Purchases

RevenueX provides complete flow for managing In App Purchase process.

Once you define your products on web panel, products will be available on SDK for further use.

- -  -

### Requesting Products

SDK will fetch the products on app launch. However if products are not pre-fetched when you request products it will start the fetch process. 

When fetch is completed, SDK will return the result to completion block.


        Revenuex.shared.getRevenueXProducts(completion: { (result) in
            switch result {
            case .success(let products):
            // Products are fetched.
            case .failure(let error):
            // Failed to fetch products.
            }
        })


- -  -

### Purchase Product

For purchasing a product, SDK provides a simple aproach. 

If you pass the selected product to purchase request, SDK will start the purchase and will return the result to completion block.

        Revenuex.shared.purchaseRevenueXProduct(product: product,completion: { (result) in
            switch result {
            case .success(let purchasedProduct):
                // Product purchased.
            case .failure(let error):
                // Failed to purchase product.
            }
        })
        
- -  -

### Checking If Purchases Are Available

In same cases, device might not be capable of accesing Apple Store.

To detect if user can purchase a product, you can use Revenuex. 

    if Revenuex.shared.canMakePayment() {
        // User is authorized to make payments 
    }
    
- -  -

## Remote Config

Remote Config gives you visibility and fine-grained control over your app's behavior and appearance so you can make changes by simply updating its configuration from the RevenueX console. 
    
- -  -

### Fetching Remote Config

To fetch remote config data from console you can use the code bellow.

    let remoteConfig = Revenuex.shared.getRemoteConfig()
    remoteConfig?.fetch(completion: { (result) in
            switch result {
            case .success(_):
                let value = remoteConfig?["key"]?.stringValue 
            case .failure(let error):
                break
            }
        })

For development purposes, there is no time limit for fetch operation.

For production, minimum fetch interval is 3 hours. If you try to fetch earlier, RevenueX will return cached data.
- -  -

### Auto Fetch

Remote config provides auto-fetch option.

This option will allow you to fetch data automaticaly when its expired. 

By default this option is enabled, to change its status you can use the code bellow.

        var remoteConfig = Revenuex.shared.getRemoteConfig()
        var settings = RemoteConfigSettings()
        settings.useAutoFetch = false
        remoteConfig.configSettings = settings
        
This option needs to be set before you call 'remoteConfig.fetch()'

If auto fetch is enabled RevenueX will post notification, when fetch operation is completed.

    NotificationCenter.default.addObserver(self, selector: #selector(configRefreshed(_:)), name: NSNotification.Name(RemoteConfigSettings.notificationKeyConfigRefreshed), object: nil)

    @objc func configRefreshed(_ notification : NSNotification) {
        let value = remoteConfig?["key"]?.stringValue 
    }
    

- -  -

### Default Values

You can define default values by adding default values as .plist file to your application. Then you can set parameters with code below.

        var remoteConfig = Revenuex.shared.getRemoteConfig()
        remoteConfig.setDefaultValues(fromPlist: "DefaultConfigFileName")
        
- -  -

### Local Source

You can define local values by adding local values as .plist file to your application. Then you can set parameters with code below.

        var remoteConfig = Revenuex.shared.getRemoteConfig()
        remoteConfig.setConfigValues(fromPlist: "LocalConfigFileName")
        
- -  -

## App User

RevenueX provides custom attributes option to detect App Users.

Using this functionalty will allow you to set unique parameters to each individual app user.

You can use these parameters on web panel to track user data.

    Revenuex.shared.setAttribute(name: "Attribute-Name", value: "Attribute-Value")
    
- -  -

