Pod::Spec.new do |s|
  s.name             = "RevenueXBeta"
  s.version          = "0.1.0"
  s.summary          = "Subscription and purchase tracking system"

  s.description      = <<-DESC
                       Secure, reliable, and free to use in-app purchase server. Build and manage your app business without having to maintain purchase infrastructure.
                       DESC

  s.homepage          = "https://bitbucket.org/mobilexturkey/revenuex-ios-sdk-public.git"
  s.license           =  { :type => 'MIT' }
  s.author            = { "Mobilex, Inc." => "support@mobilex.com.tr" }
  s.source            = { :git => "https://bitbucket.org/mobilexturkey/revenuex-ios-sdk-public.git", :tag => s.version.to_s }
  s.documentation_url = "https://bitbucket.org/mobilexturkey/revenuex-ios-sdk-public/src/master/README.md"
  s.resources         = 'RevenueX/**/*.{xcdatamodeld}'

  s.framework      = 'StoreKit'
  s.swift_version       = '5.0'
  s.ios.deployment_target = '10.0'

  # s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES' }

  s.source_files = 'RevenueX/**/*.{swift}'

end
