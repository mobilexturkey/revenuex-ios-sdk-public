//
//  RemoteConfigViewController.swift
//  revflix
//
//  Created by Dogus Yigit Ozcelik on 6.07.2021.
//

import UIKit
import RevenueXBeta

class ConfigModel {
    var value = ""
    var source = ""
}

class RemoteConfigViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var lastFetchLabel: UILabel!
    @IBOutlet weak var configTableView: UITableView!
    
    var products: [ConfigModel] = []
    
    var remoteConfig: RemoteConfig?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        remoteConfig = Revenuex.shared.getRemoteConfig()
//        let settings = RemoteConfigSettings()
        remoteConfig?.setConfigValues(fromPlist: "LocalConfig")
        remoteConfig?.setDefaultValues(fromPlist: "DefaultConfig")
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(configRefreshed(_:)), name: NSNotification.Name(RemoteConfigSettings.notificationKeyConfigRefreshed), object: nil)
    }
    
    @objc func configRefreshed(_ notification : NSNotification) {
        self.products = []
        
        let defModel = ConfigModel()
        defModel.value = self.remoteConfig?["defaultValue1"]?.stringValue ?? "def bos"
        defModel.source = self.remoteConfig?["defaultValue1"]?.source.enumDetails ?? "def bos"
        
        self.products.append(defModel)
        
        let localModel = ConfigModel()
        localModel.value = self.remoteConfig?["localValue1"]?.stringValue ?? "local bos"
        localModel.source = self.remoteConfig?["localValue1"]?.source.enumDetails ?? "local bos"
        
        self.products.append(localModel)
        
        let remoteModel = ConfigModel()
        remoteModel.value = self.remoteConfig?["super_yigit"]?.stringValue ?? "remote bos"
        remoteModel.source = self.remoteConfig?["super_yigit"]?.source.enumDetails ?? "remote bos"
        
        self.products.append(remoteModel)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        var dateStr = "--"
        if let date = self.remoteConfig?.lastFetchTime {
            dateStr = dateFormatter.string(from: date)
        }
        
        self.lastFetchLabel.text = "Last Fetch: \(dateStr)"
        
        DispatchQueue.main.async {
            self.configTableView.reloadData()
        }
    }
    
    @IBAction func fetchAction(_ sender: Any) {
        remoteConfig?.fetch(completion: { (result) in
            switch result {
            case .success(_):
                self.products = []
                
                let defModel = ConfigModel()
                defModel.value = self.remoteConfig?["defaultValue1"]?.stringValue ?? "def bos"
                defModel.source = self.remoteConfig?["defaultValue1"]?.source.enumDetails ?? "def bos"
                
                self.products.append(defModel)
                
                let localModel = ConfigModel()
                localModel.value = self.remoteConfig?["localValue1"]?.stringValue ?? "local bos"
                localModel.source = self.remoteConfig?["localValue1"]?.source.enumDetails ?? "local bos"
                
                self.products.append(localModel)
                
                let remoteModel = ConfigModel()
                remoteModel.value = self.remoteConfig?["super_yigit"]?.stringValue ?? "remote bos"
                remoteModel.source = self.remoteConfig?["super_yigit"]?.source.enumDetails ?? "remote bos"
                
                self.products.append(remoteModel)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
                var dateStr = "--"
                if let date = self.remoteConfig?.lastFetchTime {
                    dateStr = dateFormatter.string(from: date)
                }
                
                self.lastFetchLabel.text = "Last Fetch: \(dateStr)"
                
                DispatchQueue.main.async {
                    self.configTableView.reloadData()
                }
            
            case .failure(let error):
                break
            }
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        NotificationCenter.default.removeObserver(self)
        self.dismiss(animated: true, completion: nil)
    }

}

extension RemoteConfigViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
//        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let product = products[(indexPath as NSIndexPath).row]
//
        cell.textLabel?.text = "\(product.value) -- \(product.source)"

        return cell
    }
}
