//
//  ViewController.swift
//  revflix
//
//  Created by Orhan DALGARA on 17.03.2021.
//

import UIKit
import RevenueXBeta
import StoreKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var keyTextFied: UITextField!
    @IBOutlet weak var valueTextField: UITextField!
    
    @IBOutlet weak var productsTableView: UITableView!
    var products: [RevenueXProduct] = []
    
    private var productsRequest: SKProductsRequest?
    var productsArr : [SKProduct] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        productsRequest?.cancel()
        productsRequest = SKProductsRequest(productIdentifiers: ["com.mobilex.revenuex.revflix.consumablet1"])
        productsRequest!.delegate = self
        productsRequest!.start()
    }
    
    @IBAction func showRemoteConfig(_ sender: Any) {
        self.performSegue(withIdentifier: "showRemoteConfig", sender: nil)
    }
    
    @IBAction func setButtonTapped(_ sender: Any) {
        Revenuex.shared.setAttribute(name: (keyTextFied.text ?? "email"), value: (valueTextField.text ?? "test@mobilex.com.tr"))
    }
    
    @IBAction func loadProds(_ sender: Any) {
        Revenuex.shared.getRevenueXProducts(completion: { (result) in
            switch result {
            case .success(let products):
                self.products = products
                DispatchQueue.main.async {
                    self.productsTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        })
    }
    
    @IBAction func buyWithStoreKit(_ sender: Any) {
        let payment = SKPayment(product: productsArr[0])
        SKPaymentQueue.default().add(payment)
    }
}

// MARK: - SKProductsRequestDelegate

extension ViewController: SKProductsRequestDelegate {

    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded list of products...")
        let products = response.products
        
        for p in products {
            productsArr.append(p)
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
        clearRequestAndHandler()
    }

    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        clearRequestAndHandler()
    }

    private func clearRequestAndHandler() {
        productsRequest = nil
    }
}

// MARK: - SKPaymentTransactionObserver

extension ViewController: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            @unknown default:
                break
            }
        }
  }

    private func complete(transaction: SKPaymentTransaction) {
        print("complete...")
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        print("restore... \(productIdentifier)")
        SKPaymentQueue.default().finishTransaction(transaction)
    }

    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as NSError?,
           let localizedDescription = transaction.error?.localizedDescription,
           transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }
}


// MARK: - UITableViewDataSource

extension ViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let product = products[(indexPath as NSIndexPath).row]

        cell.textLabel?.text = product.localizedTitle

        return cell
    }
}

// MARK: - UITableViewDelegate

extension ViewController {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = products[(indexPath as NSIndexPath).row]
        
        Revenuex.shared.purchaseRevenueXProduct(product: product,completion: { (result) in
            switch result {
            case .success(let products):
                break
            case .failure(let error):
                break
            }
        })
    }
    
}
