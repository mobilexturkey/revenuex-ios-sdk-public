//
//  SetSDKViewController.swift
//  revflix
//
//  Created by Dogus Yigit Ozcelik on 6.07.2021.
//

import UIKit
import RevenueXBeta

class SetSDKViewController: UIViewController {

    @IBOutlet weak var urlTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        urlTextField.text = UserDefaults.standard.string(forKey: "revenuexIdURL") ?? ""
    }
    

    @IBAction func setSDK(_ sender: Any) {
        let url = urlTextField.text ?? ""
        UserDefaults.standard.setValue(url, forKey: "revenuexIdURL")
        
        Revenuex.shared.configure(with: "AA61369F5A82980891DF9073")
        
        let remoteConfig = Revenuex.shared.getRemoteConfig()
//        let settings = RemoteConfigSettings()
        remoteConfig.setConfigValues(fromPlist: "LocalConfig")
        remoteConfig.setDefaultValues(fromPlist: "DefaultConfig")
        
//        remoteConfig = RemoteConfig.remoteConfig()
//        let settings = RemoteConfigSettings()
//        settings.minimumFetchInterval = 0
//        remoteConfig.configSettings = settings
//        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
//        fetchConfig()
        
        self.performSegue(withIdentifier: "showMainPage", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
